const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	buyerId:{
		type:String,
		required: [true, "BuyerID is required"]
	},
	totalAmount:{
		type:Number,
		required: [true, "Total Amount is required"]
	},
	purchasedOn:{
		type: Date,
		default: new Date()
	},
	productsToBuy:[{
		productId:{
			type: String,
			required: [true, "Product ID is required."]
		},
		name:{
			type: String,
			required: [true, "Product name is required."]
		},
		quantity:{
			type: Number,
			required: [true, "Quantity is required."]
		},
		price:{
			type: Number,
			required:[true, "Product price is required."]
		}
	}]
})

module.exports = mongoose.model("Order",orderSchema)