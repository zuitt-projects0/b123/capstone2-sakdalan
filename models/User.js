const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	fName : {
		type : String,
		required : [true, "First name is required"]
	},
	lName : {
		type : String,
		required : [true, "Last name is required"]
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	}
})

module.exports = mongoose.model("User", userSchema)
