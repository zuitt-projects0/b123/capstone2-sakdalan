const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Name is required"]
	},
	desc:{
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	image:{
		type: String,
		required: [true, "Image is required"]
	},
	isFigma:{
		type: Boolean,
		default: false
	},
	isFunko:{
		type: Boolean,
		default: false
	},	
	isNendo:{
		type: Boolean,
		default: false
	},
	isActive:{
		type: Boolean,
		default: true
	},	
	createdOn:{
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Product",productSchema)