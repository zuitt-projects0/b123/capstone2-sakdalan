const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const orderControllers = require('../controllers/orderControllers')
const{
	userRegister,
	userLogin,
	userSetAdmin,
	getSingleUser
}=userControllers

const auth = require('../auth')
const {verify,verifyAdmin} = auth

router.post('/register',userRegister)

router.post('/login',userLogin)

router.get('/getUserDetails',verify,getSingleUser);

router.put('/setAsAdmin/:id',verify,verifyAdmin,userSetAdmin)

module.exports = router


