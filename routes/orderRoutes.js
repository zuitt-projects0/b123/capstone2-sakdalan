const express = require('express')
const router = express.Router()
const orderControllers = require('../controllers/orderControllers')
const {
	createOrder,
	retrieveAllOrder,
	retrieveAuthOrder,
	displayPerOrder
} = orderControllers

const auth = require('../auth')
const {verify,verifyAdmin, verifyNonAdmin} = auth

router.post('/createOrder',verify,createOrder)

router.get('/retrieveAllOrder',verify,verifyAdmin,retrieveAllOrder)

router.get('/retrieveAuthOrder',verify,verifyNonAdmin,retrieveAuthOrder)

//router.get('/displayPerOrder',verify,verifyNonAdmin,displayPerOrder)

module.exports = router