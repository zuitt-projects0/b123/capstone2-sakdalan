const express = require('express')
const router = express.Router()
const productControllers = require('../controllers/productControllers')
const {
	createProduct,
	retrieveAllActive,
	getSingleProduct,
	getSingleProductNon,
	getAllProduct,
	getAllFigma,
	getAllNendo,
	getAllFunko,
	updateProduct,
	productArchive,
	activateProduct,
	deactivateProduct
} = productControllers

const auth = require('../auth')
const {verify,verifyAdmin} = auth

router.post('/createProduct',verify,verifyAdmin,createProduct)

router.get('/retrieveAllActive',retrieveAllActive)

router.get('/getAllProduct',verify,verifyAdmin,getAllProduct)

router.get('/getAllFigma',getAllFigma)

router.get('/getAllNendo',getAllNendo)

router.get('/getAllFunko',getAllFunko)

router.get('/getSingleProductNon/:id',verify,getSingleProductNon)

router.get('/getSingleProduct/:id',verify,verifyAdmin,getSingleProduct)

router.put('/updateProduct/:id',verify,verifyAdmin,updateProduct)

router.put('/productArchive/:id',verify,verifyAdmin,productArchive)

router.put('/activateProduct/:id',verify,verifyAdmin,activateProduct)

router.put('/deactivateProduct/:id',verify,verifyAdmin,deactivateProduct)

module.exports = router