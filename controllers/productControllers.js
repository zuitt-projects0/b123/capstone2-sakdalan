const Product = require('../models/Product')

module.exports.createProduct = (req,res) => {
	let newProd = new Product({
		name: req.body.name,
		desc: req.body.desc,
		price: req.body.price,
		image: req.body.image,
		isFigma: req.body.isFigma,
		isFunko: req.body.isFunko,
		isNendo: req.body.isNendo,
		isActive: req.body.isActive
	})
	newProd.save()
	.then(prod => res.send(prod))
	.catch(err => res.send(err))
}

module.exports.retrieveAllActive = (req,res) => {
	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(err => res.status(500).send)
}

module.exports.getSingleProduct = (req,res) => {
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getSingleProductNon = (req,res) => {
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateProduct = (req,res) => {
	let updateProd = {
		name:req.body.name,
		desc: req.body.desc,
		price: req.body.price,
		image: req.body.image,
		isActive: req.body.isActive
	}
	Product.findByIdAndUpdate(req.params.id, updateProd, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.productArchive = (req,res) =>{
	let updateProd = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.id, updateProd, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.activateProduct= (req,res) => {
	let updatedProduct = {
		isActive: true
	}
	Product.findByIdAndUpdate(req.params.id,updatedProduct,{new:true})
	.then(activatedProd => res.send(activatedProd))
	.catch(error => res.send(error))
}

module.exports.getAllProduct = (req,res) => {
	Product.find({})
	.then(result => res.send(result))
}

module.exports.deactivateProduct = (req,res) => {
	let deacProd = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.id,deacProd,{new:true})
	.then(result => {
		console.log(result)
		res.send(result)}
		)
	.catch(err => res.send(err))
}

module.exports.getAllFigma = (req,res) => {
	Product.find({isFigma: true})
	.then(result => res.send(result))
	.catch(err => res.status(500).send)
}

module.exports.getAllNendo = (req,res) => {
	Product.find({isNendo: true})
	.then(result => res.send(result))
	.catch(err => res.status(500).send)
}

module.exports.getAllFunko = (req,res) => {
	Product.find({isFunko: true})
	.then(result => res.send(result))
	.catch(err => res.status(500).send)
}