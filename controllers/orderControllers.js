const Order = require('../models/Order')
const User = require('../models/User')
const Product = require('../models/Product')


module.exports.createOrder = (req,res) => {
	console.log(req.user)
	let newOrder = new Order({
		buyerId: req.user.id,
		totalAmount: req.body.totalAmount
	})
	req.body.productsToBuy.forEach(function(productsToBuyParam){
		newOrder.productsToBuy.push(productsToBuyParam) 
	})
	newOrder.save()
	.then(ord => res.send(ord))
	.catch(err => res.send(err))
}

module.exports.retrieveAllOrder = (req,res) => {
	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.retrieveAuthOrder = (req,res) => {
	Order.find({buyerId: req.user.id})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

/*module.exports.displayPerOrder = (req,res) => {
	Order.find({buyerId: req.user.id})
	.then(result => {
		result.productsToBuy.forEach(function(params){
			console.log(result.params)
		})
		res.send(result.productsToBuy[1])
	}
	)
	.catch(err => res.send(err))
}*/