const User = require('../models/User')
const Order = require("../models/Order")
const Product = require("../models/Product")
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {createAccessToken} = auth


module.exports.userRegister = async (req,res) => {
	try{
		if(req.body.password.length < 8){
			return res.send({message: "Password is too short."})
		}

		const hashedPW = bcrypt.hashSync(req.body.password,10)

		console.log(hashedPW)

		let newUser = new User({
			fName: req.body.fName,
			lName: req.body.lName,
			email: req.body.email,
			password: hashedPW,
			mobileNo: req.body.mobileNo,
			isAdmin: req.body.isAdmin
		})

		const savedUser = await newUser.save()
		res.send(savedUser)
	}
	catch(error){
		 res.send(error)
	}
}

module.exports.userLogin = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({
				message:"No User Found."
			})	
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password)
			console.log(isPasswordCorrect)

			if(isPasswordCorrect){
				return res.send({accessToken: createAccessToken(result)})
			}
			else{
				return res.send({message: "Password is incorrect."})
			}
		}
	})
	.catch(err => res.send(err))
}


module.exports.userSetAdmin = (req,res) => {
	let updateAdmin = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.id,updateAdmin,{new:true})
	.then(result => {return res.send({message:"Admin Credentials: ", email: result.email, password: result.password})})
}

module.exports.getSingleUser = (req,res) => {
	console.log(req.user);
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

